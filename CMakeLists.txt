cmake_minimum_required(VERSION 3.10)
project(TicTacToe)

set(CMAKE_C_STANDARD 99)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -pedantic -Wall -Wextra")

ADD_SUBDIRECTORY(engine)
ADD_SUBDIRECTORY(interface)