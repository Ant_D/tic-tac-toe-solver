//
// Created by Anton on 06.06.2018.
//

#include "playGtp.h"

#include <stdlib.h>
#include <string.h>

#include "engine.h"
#include "gtp.h"
#include "position.h"
#include "random.h"
#include "zobristHash.h"

/* Avoid compiler warnings with unused parameters */
#define UNUSED(x)  (void)x

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

static int gtpClearBoard(char *s);
static int gtpGenMove(char *s);
static int gtpKnownCommand(char *s);
static int gtpListCommands(char *s);
static int gtpName(char *s);
static int gtpPlay(char *s);
static int gtpProtocolVersion(char *s);
static int gtpRegShowPositionHashKey(char *s);
static int gtpSetBoardSize(char *s);
static int gtpSetUnitLength(char *s);
static int gtpShowBoard(char *s);
static int gtpUndo(char *s);
static int gtpQuit(char *s);
static int gtpVersion(char *s);

static struct gtp_command commands[] = {
        {"boardsize", gtpSetBoardSize},
        {"clear_board", gtpClearBoard},
        {"genmove", gtpGenMove},
        {"help", gtpListCommands},
        {"known_command", gtpKnownCommand},
        {"list_commands", gtpListCommands},
        {"name", gtpName},
        {"play", gtpPlay},
        {"protocol_version", gtpProtocolVersion},
        {"reg_show_position_hash_key", gtpRegShowPositionHashKey},
        {"showboard", gtpShowBoard},
        {"quit", gtpQuit},
        {"undo", gtpUndo},
        {"unitlength", gtpSetUnitLength},
        {"version", gtpVersion},
        {NULL, NULL}
};

static bool gtpGameStarted = false;
static size_t gtpBoardSize = 0;

Engine_T engine;

void playGtp(FILE *gtpInput, FILE *gtpOutput, FILE *gtpDumpCommands) {
    gg_srand(1);
    zobristHashInit();
    gtp_main_loop(commands, gtpInput, gtpOutput, gtpDumpCommands);
}

static int gtpQuit(char *s) {
    UNUSED(s);
    gtp_success("");
    return GTP_QUIT;
}

static int gtpProtocolVersion(char *s) {
    UNUSED(s);
    return gtp_success("%d", GTP_VERSION);
}

static int gtpName(char *s) {
    UNUSED(s);
    return gtp_success("TicTacToe");
}

static int gtpVersion(char *s) {
    UNUSED(s);
    return gtp_success("0.0.0");
}

static int gtpKnownCommand(char *s) {
    char cmd[GTP_BUFSIZE];

    if (sscanf(s, "%s", cmd) == 1)
        for (int i = 0; commands[i].name != NULL; ++i)
            if (!strcmp(commands[i].name, cmd))
                return gtp_success("true");
    return gtp_success("false");
}

static int gtpListCommands(char *s) {
    UNUSED(s);

    gtp_start_response(GTP_SUCCESS);

    for (int i = 0; commands[i].name != NULL; ++i)
        gtp_printf("%s\n", commands[i].name);
    gtp_printf("\n");

    return GTP_OK;
}

static int gtpSetBoardSize(char *s) {
    size_t boardSize;

    if (sscanf(s, "%u", &boardSize) != 1)
        return gtp_failure("can't read boardsize");

    if (!(MIN_POSITION_WIDTH <= boardSize && boardSize <= MAX_POSITION_WIDTH))
        return gtp_failure("boardsize should be from [%d; %d]", MIN_POSITION_WIDTH, MAX_POSITION_WIDTH);

    gtpGameStarted = false;
    gtp_internal_set_boardsize(boardSize);
    gtpBoardSize = boardSize;

    return gtp_success("");
}

static int gtpClearBoard(char *s) {
    FUNC_RESULT result;

    UNUSED(s);

    result = PositionClear(&engine.Position);

    if (FUNC_RESULT_SUCCESS != result)
        return gtp_failure("failed to clear board");

    return gtp_success("");
}

static int gtpPlay(char *s) {
    bool isPass;
    int i, j, color;
    FUNC_RESULT result;

    if (!gtpGameStarted)
        return gtp_failure("game has not started yet");

    if (!gtp_decode_move(s, &color, &i, &j))
        return gtp_failure("invalid color or coordinate");

    if (NULL == strstr(s, "resign")) {
        isPass = (bool)strstr(s, "pass");

        Move_T move = {i, j, color == BLACK ? SIDE_BLACK : SIDE_WHITE, isPass};
        result = PositionDoMove(&engine.Position, &move);

        if (FUNC_RESULT_SUCCESS != result)
            return gtp_failure("failed to play");
    }

    return gtp_success("");
}

static int gtpGenMove(char *s) {
    int color;
    side_T side;
    Move_T move;
    FUNC_RESULT result;

    if (!gtpGameStarted)
        return gtp_failure("game has not started yet");

    if (!gtp_decode_color(s, &color))
        return gtp_failure("invalid color");

    side = color == BLACK ? SIDE_BLACK : SIDE_WHITE;

    result = EngineGenMove(&engine, side, &move);
    if (FUNC_RESULT_SUCCESS != result)
        return gtp_failure("failed to generate move");

    result = PositionDoMove(&engine.Position, &move);
    if (FUNC_RESULT_SUCCESS != result)
        return gtp_failure("failed to play");

    gtp_start_response(GTP_SUCCESS);
    gtp_print_vertex(move.RowIndex, move.ColumnIndex);
    return gtp_finish_response();
}

static int gtpSetUnitLength(char *s) {
    size_t unitLength;
    FUNC_RESULT result;

    if (0 == gtpBoardSize)
        return gtp_failure("set board size");

    if (sscanf(s, "%u", &unitLength) != 1)
        return gtp_failure("can't read unit length");

    if (!(MIN_UNIT_LENGTH <= unitLength && unitLength <= MIN(MAX_UNIT_LENGTH, gtpBoardSize)))
        return gtp_failure("unit length should be from [%d; %d]",
                           MIN_UNIT_LENGTH, MIN(MAX_UNIT_LENGTH, gtpBoardSize));

    result = PositionInit(&engine.Position, gtpBoardSize, gtpBoardSize, unitLength);
    if (FUNC_RESULT_SUCCESS != result)
        return gtp_failure("can not initialize position");

    gtpGameStarted = true;

    return gtp_success("");
}

static int gtpShowBoard(char *s) {
    UNUSED(s);

    if (!gtpGameStarted)
        return gtp_failure("game has not started yet");

    gtp_start_response(GTP_SUCCESS);
    PositionPrint(&engine.Position, gtp_output_file);
    return gtp_finish_response();
}

int gtpUndo(char *s) {
    FUNC_RESULT result;
    UNUSED(s);

    if (!gtpGameStarted)
        return gtp_failure("game has not started yet");

    result = PositionUndo(&engine.Position);

    if (FUNC_RESULT_SUCCESS != result)
        return gtp_failure("failed to undo");
    return gtp_success("");
}

int gtpRegShowPositionHashKey(char *s) {
    UNUSED(s);

    if (!gtpGameStarted)
        return gtp_failure("game has not started yet");

    return gtp_success("%I64u", engine.Position.hashKey);
}
