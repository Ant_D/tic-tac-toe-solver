//
// Created by Anton on 06.06.2018.
//

#ifndef TICTACTOE_PLAY_GTP_H
#define TICTACTOE_PLAY_GTP_H

#include <stdio.h>

#include "engine.h"

void playGtp(FILE *gtpInput, FILE *gtpOutput, FILE *gtpDumpCommands);

#endif //TICTACTOE_PLAY_GTP_H
