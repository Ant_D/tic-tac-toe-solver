//
// Created by Anton on 08.06.2018.
//

#ifndef TICTACTOE_HASH_H
#define TICTACTOE_HASH_H

#include <stdint.h>

#include "const.h"
#include "funcResults.h"

typedef uint64_t hashKey_T;

FUNC_RESULT zobristHashInit();

FUNC_RESULT zobristHashPlaceColor(hashKey_T *hashKey, size_t rowIndex, size_t columnIndex, side_T oldColor, side_T newColor);

FUNC_RESULT zobristHashChangeNextPlayer(hashKey_T *hashKey);

#endif //TICTACTOE_HASH_H
