//
// Created by Anton on 10.06.2018.
//

#ifndef TICTACTOE_ENGINE_H
#define TICTACTOE_ENGINE_H

#include "position.h"

typedef struct {
    Position_T Position;
} Engine_T;

FUNC_RESULT EngineGenMove(Engine_T *engine, side_T side, Move_T *move);

#endif //TICTACTOE_ENGINE_H
