//
// Created by Anton on 05.06.2018.
//

#ifndef TICTACTOE_POSITION_H
#define TICTACTOE_POSITION_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "const.h"
#include "funcResults.h"
#include "zobristHash.h"

typedef int64_t value_T;

typedef struct {
    size_t RowIndex;
    size_t ColumnIndex;
    side_T Color;
    bool IsPass;
} Move_T;

typedef struct {
    size_t BlackCount;
    size_t WhiteCount;
    size_t Cells[MAX_UNIT_LENGTH];
} Unit_T;

typedef struct {
    size_t RowsCount;
    size_t ColumnsCount;
    size_t UnitLength;
    size_t Size;
    size_t MoveHistorySize;
    hashKey_T hashKey;
    side_T Position[MAX_POSITION_SZ];
    Move_T MoveHistory[MAX_MOVE_HISTORY_SZ];
    size_t UnitsCount;
    Unit_T Units[MAX_UNIT_COUNT];
    size_t CellToUnitsCount[MAX_POSITION_SZ];
    Unit_T *CellToUnits[MAX_POSITION_SZ][MAX_UNIT_COUNT_PER_CELL];
} Position_T;

Position_T *PositionCreate(size_t rowsCount, size_t columnsCount, size_t unitLength);

FUNC_RESULT PositionInit(Position_T *position, size_t rowsCount, size_t columnsCount, size_t unitLength);

FUNC_RESULT PositionClear(Position_T *position);

FUNC_RESULT PositionDestroy(Position_T *position);

FUNC_RESULT PositionPlaceColor(Position_T *position, size_t rowIndex, size_t columnIndex, side_T color);

FUNC_RESULT PositionDoMove(Position_T *position, const Move_T *move);

FUNC_RESULT PositionUndo(Position_T *position);

side_T PositionGet(const Position_T *position, size_t rowIndex, size_t columnIndex);

FUNC_RESULT PositionPrint(const Position_T *position, FILE *out);

bool PositionIsGameOver(const Position_T *position, side_T *winner);

bool PositionMoveIsWinning(const Position_T *position, size_t rowIndex, size_t columnIndex, side_T color);

FUNC_RESULT PositionGetCellValue(const Position_T *position, size_t rowIndex, size_t columnIndex, side_T side, value_T *value);

#endif //TICTACTOE_POSITION_H
