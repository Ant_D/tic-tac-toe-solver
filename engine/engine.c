//
// Created by Anton on 10.06.2018.
//

#include "engine.h"
#include "position.h"
#include "random.h"


FUNC_RESULT EngineGenMove(Engine_T *engine, side_T side, Move_T *moveAnswer) {
    static size_t moveCandidatesCount, legalMovesCount;
    static const Move_T *moveCandidates[MAX_POSITION_SZ];
    static Move_T legalMoves[MAX_POSITION_SZ];
    const Position_T *position;
    side_T enemySide;
    value_T bestValue, value;
    const Move_T *move;
    side_T sides[2];

    if (NULL == engine || NULL == moveAnswer ||
        (SIDE_BLACK != side && SIDE_WHITE != side))
        return FUNC_RESULT_FAILED_ARGUMENT;

    position = &engine->Position;
    enemySide = SIDE_BLACK == side ? SIDE_WHITE : SIDE_BLACK;

    legalMovesCount = 0;
    for (size_t rowIndex = 0; rowIndex < position->RowsCount; ++rowIndex)
        for (size_t columnIndex = 0; columnIndex < position->ColumnsCount; ++columnIndex)
            if (SIDE_NONE == PositionGet(position, rowIndex, columnIndex)) {
                legalMoves[legalMovesCount].RowIndex = rowIndex;
                legalMoves[legalMovesCount].ColumnIndex = columnIndex;
                legalMoves[legalMovesCount].Color = side;
                legalMoves[legalMovesCount].IsPass = false;
                ++legalMovesCount;
            }

    sides[0] = side;
    sides[1] = enemySide;
    for (int i = 0; i < 2; ++i) {
        moveCandidatesCount = 0;
        for (size_t index= 0; index < legalMovesCount; ++index) {
            move = legalMoves + index;
            if (PositionMoveIsWinning(position, move->RowIndex, move->ColumnIndex, sides[i]))
                moveCandidates[moveCandidatesCount++] = move;
        }

        if (0 < moveCandidatesCount) {
            *moveAnswer = *moveCandidates[gg_rand() % moveCandidatesCount];
            return FUNC_RESULT_SUCCESS;
        }
    }

    bestValue = 0;
    moveCandidatesCount = 0;
    for (size_t index= 0; index < legalMovesCount; ++index) {
        move = legalMoves + index;
        PositionGetCellValue(position, move->RowIndex, move->ColumnIndex, SIDE_BOTH, &value);
        if (0 == moveCandidatesCount || value == bestValue)
            moveCandidates[moveCandidatesCount++] = move;
        else if (bestValue < value) {
            bestValue = value;
            moveCandidatesCount = 1;
            moveCandidates[moveCandidatesCount] = move;
        }
    }

    if (0 < moveCandidatesCount)
        *moveAnswer = *moveCandidates[gg_rand() % moveCandidatesCount];
    else {
        moveAnswer->Color = side;
        moveAnswer->IsPass = true;
    }

    return FUNC_RESULT_SUCCESS;
}
