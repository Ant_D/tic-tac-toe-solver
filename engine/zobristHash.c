//
// Created by Anton on 08.06.2018.
//

#include "zobristHash.h"

#include <stdlib.h>

#include "position.h"
#include "random.h"

#define HASH_KEYS_CNT (2 * MAX_POSITION_SZ + 1)

static hashKey_T hashKeys[HASH_KEYS_CNT];

static hashKey_T zobristHashRand() {
    return (hashKey_T) (gg_urand() & 0xffffffffU) << 32 | (gg_urand() & 0xffffffff);
}

static hashKey_T zobristHashIndex(size_t rowIndex, size_t columnIndex, side_T color) {
    return (rowIndex * MAX_POSITION_WIDTH + columnIndex) + (color == SIDE_BLACK ? 0 : 1) * MAX_POSITION_SZ;
}

static hashKey_T zobristHashKey(size_t rowIndex, size_t columnIndex, side_T color) {
    return hashKeys[zobristHashIndex(rowIndex, columnIndex, color)];
}

static hashKey_T zobristHashNextPlayerKey() {
    return hashKeys[HASH_KEYS_CNT - 1];
}

static bool correctColor(char color) {
    return color == SIDE_NONE || color == SIDE_BLACK || color == SIDE_WHITE;
}

FUNC_RESULT zobristHashInit() {
    for (size_t index = 0; index < HASH_KEYS_CNT; ++index)
        hashKeys[index] = zobristHashRand();

    return FUNC_RESULT_SUCCESS;
}

FUNC_RESULT zobristHashPlaceColor(hashKey_T *hashKey, size_t rowIndex, size_t columnIndex, side_T oldColor, side_T newColor) {
    if (NULL == hashKey || rowIndex >= MAX_POSITION_WIDTH || columnIndex >= MAX_POSITION_WIDTH ||
        !correctColor(oldColor) || !correctColor(newColor))
        return FUNC_RESULT_FAILED_ARGUMENT;

    if (oldColor != SIDE_NONE)
        *hashKey ^= zobristHashKey(rowIndex, columnIndex, oldColor);

    if (newColor != SIDE_NONE)
        *hashKey ^= zobristHashKey(rowIndex, columnIndex, newColor);

    return FUNC_RESULT_SUCCESS;
}

FUNC_RESULT zobristHashChangeNextPlayer(hashKey_T *hashKey) {
    if (NULL == hashKey)
        return FUNC_RESULT_FAILED_ARGUMENT;

    *hashKey ^= zobristHashNextPlayerKey();

    return FUNC_RESULT_SUCCESS;
}





