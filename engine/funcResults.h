//
// Created by Anton on 05.06.2018.
//

#ifndef TICTACTOE_FUNCRESULTS_H
#define TICTACTOE_FUNCRESULTS_H

#define FUNC_RESULT_SUCCESS						0
#define FUNC_RESULT_FAILED						-1
#define FUNC_RESULT_FAILED_ARGUMENT				-2
#define FUNC_RESULT_FAILED_IO					-3
#define FUNC_RESULT_FAILED_MEM_ALLOCATION		-4

#define CHECK_RESULT(r)				do{ int __res = (r); if( FUNC_RESULT_SUCCESS != __res ) return __res; }while( 0 )

typedef int	FUNC_RESULT;


#endif //TICTACTOE_FUNCRESULTS_H
