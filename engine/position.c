//
// Created by Anton on 05.06.2018.
//

#include "position.h"

#include <stdlib.h>
#include <memory.h>

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define ABS(a) (((a) < 0) ? -(a) : (a))

#define INITIATIVE_VALUE 1

static const value_T VALUE_BASE = MAX(100, MAX_UNIT_LENGTH * 4 + INITIATIVE_VALUE + 1);

static bool valuePowInitialized = false;
static value_T valuePow[MAX_UNIT_LENGTH + 1];

static void valuePowInit() {
    valuePowInitialized = true;
    valuePow[0] = 1;
    for (int i = 1; i <= MAX_UNIT_LENGTH; ++i)
        valuePow[i] = valuePow[i - 1] * VALUE_BASE;
}

static side_T UnitSide(const Unit_T *unit) {
    if (unit->BlackCount + unit->WhiteCount == 0)
        return SIDE_NONE;
    if (0 < unit->BlackCount && 0 < unit->WhiteCount)
        return SIDE_BOTH;
    if (0 < unit->BlackCount)
        return SIDE_BLACK;
    else
        return SIDE_WHITE;
}

static value_T UnitValue(const Unit_T *unit) {
    switch (UnitSide(unit)) {
        case SIDE_NONE: return 1;
        case SIDE_BLACK: return valuePow[unit->BlackCount];
        case SIDE_WHITE: return -valuePow[unit->WhiteCount];
        default: return 0;
    }
}

static bool correctColor(side_T color) {
    return color == SIDE_NONE || color == SIDE_BLACK || color == SIDE_WHITE;
}

static inline bool PositionInside(const Position_T *position, int rowIndex, int columnIndex) {
    return 0 <= rowIndex && rowIndex < (int)position->RowsCount &&
           0 <= columnIndex && columnIndex < (int)position->ColumnsCount;
}

static bool PositionCorrectMove(const Position_T *position, const Move_T *move) {
    return move->IsPass || (PositionInside(position, move->RowIndex, move->ColumnIndex) &&
            (position->MoveHistorySize % 2 == 0 ? move->Color == SIDE_BLACK : move->Color == SIDE_WHITE) &&
            PositionGet(position, move->RowIndex, move->ColumnIndex) == SIDE_NONE);
}

static inline size_t PositionIndex(const Position_T *position, size_t rowIndex, size_t columnIndex) {
    return position->ColumnsCount * rowIndex + columnIndex;
}

static FUNC_RESULT PositionAddMoveToMoveHistory(Position_T *position, const Move_T *move) {
    Move_T *nextMove;

    if (NULL == position || NULL == move)
        return FUNC_RESULT_FAILED_ARGUMENT;

    if (position->MoveHistorySize >= MAX_MOVE_HISTORY_SZ)
        return FUNC_RESULT_FAILED;

    nextMove = position->MoveHistory + position->MoveHistorySize;
    *nextMove = *move;
    ++position->MoveHistorySize;

    return FUNC_RESULT_SUCCESS;
}

static void PositionInitUnits(Position_T *position) {
    const int X_DIR[] = {1, 1, 0, -1},
            Y_DIR[] = {0, 1, 1, 1};
    size_t cell;
    Unit_T *unit;

    position->UnitsCount = 0;
    memset(position->CellToUnitsCount, 0, sizeof(position->CellToUnitsCount));

    for (size_t rowIndex = 0; rowIndex < position->RowsCount; ++rowIndex)
        for (size_t columnIndex = 0; columnIndex < position->ColumnsCount; ++columnIndex)
            for (int dir = 0; dir < 4; ++dir)
                if (PositionInside(position,
                                   (int)rowIndex + (position->UnitLength - 1) * Y_DIR[dir],
                                   (int)columnIndex + (position->UnitLength - 1) * X_DIR[dir])) {
                    unit = position->Units + position->UnitsCount;
                    ++position->UnitsCount;
                    for (size_t i = 0; i < position->UnitLength; ++i) {
                        cell = PositionIndex(position,
                                             rowIndex + i * Y_DIR[dir],
                                             columnIndex + i * X_DIR[dir]);
                        unit->Cells[i] = cell;
                        position->CellToUnits[cell][position->CellToUnitsCount[cell]++] = unit;
                    }
                }
}

static void PositionUpdateUnits(Position_T *position, size_t rowIndex, size_t columnIndex, side_T oldColor, side_T newColor) {
    Unit_T *unit;
    size_t cell = PositionIndex(position, rowIndex, columnIndex);

    for (size_t index = 0; index < position->CellToUnitsCount[cell]; ++index) {
        unit = position->CellToUnits[cell][index];
        if (SIDE_BLACK == oldColor)
            --unit->BlackCount;
        else if (SIDE_WHITE == oldColor)
            --unit->WhiteCount;
        if (SIDE_BLACK == newColor)
            ++unit->BlackCount;
        else if (SIDE_WHITE == newColor)
            ++unit->WhiteCount;
    }
}

Position_T *PositionCreate(size_t rowsCount, size_t columnsCount, size_t unitLength) {
    Position_T *position = (Position_T *) malloc(sizeof(Position_T));

    PositionInit(position, rowsCount, columnsCount, unitLength);

    return position;
}

FUNC_RESULT PositionInit(Position_T *position, size_t rowsCount, size_t columnsCount, size_t unitLength) {
    if (NULL == position || !(MIN_POSITION_WIDTH <= rowsCount && rowsCount <= MAX_POSITION_WIDTH) ||
        !(MIN_POSITION_WIDTH <= columnsCount && columnsCount <= MAX_POSITION_WIDTH) ||
        !(MIN_UNIT_LENGTH <= unitLength && unitLength <= MIN(MAX_UNIT_LENGTH, MAX(rowsCount, columnsCount))))
        return FUNC_RESULT_FAILED_ARGUMENT;

    if (!valuePowInitialized) {
        valuePowInitialized = true;
        valuePowInit();
    }

    position->RowsCount = rowsCount;
    position->ColumnsCount = columnsCount;
    position->Size = rowsCount * columnsCount;
    position->UnitLength = unitLength;

    PositionInitUnits(position);
    PositionClear(position);

    return FUNC_RESULT_SUCCESS;
}

FUNC_RESULT PositionClear(Position_T *position) {
    if (NULL == position)
        return FUNC_RESULT_FAILED_ARGUMENT;

    for (Unit_T *unit = position->Units; unit - position->Units < (int)position->UnitsCount; ++unit) {
        unit->WhiteCount = unit->BlackCount = 0;
    }

    position->MoveHistorySize = 0;
    position->hashKey = 0;
    for (size_t index = 0; index < position->Size; ++index)
        position->Position[index] = SIDE_NONE;

    return FUNC_RESULT_SUCCESS;
}

FUNC_RESULT PositionDestroy(Position_T *position) {
    if (NULL == position)
        return FUNC_RESULT_FAILED_ARGUMENT;

    free(position);

    return FUNC_RESULT_SUCCESS;
}

FUNC_RESULT PositionPlaceColor(Position_T *position, size_t rowIndex, size_t columnIndex, side_T color) {
    int index;
    side_T oldColor;
    FUNC_RESULT result;

    if (NULL == position || !PositionInside(position, rowIndex, columnIndex) || !correctColor(color))
        return FUNC_RESULT_FAILED_ARGUMENT;

    index = PositionIndex(position, rowIndex, columnIndex);
    oldColor = position->Position[index];

    PositionUpdateUnits(position, rowIndex, columnIndex, oldColor, color);

    result = zobristHashPlaceColor(&position->hashKey, rowIndex, columnIndex, oldColor, color);
    if (FUNC_RESULT_SUCCESS != result)
        return result;

    position->Position[index] = color;

    return result;
}

FUNC_RESULT PositionUndo(Position_T *position) {
    const Move_T *move;
    FUNC_RESULT result;

    if (NULL == position)
        return FUNC_RESULT_FAILED_ARGUMENT;

    if (0 == position->MoveHistorySize)
        return FUNC_RESULT_FAILED;

    move = position->MoveHistory + position->MoveHistorySize - 1;
    --position->MoveHistorySize;

    result = zobristHashChangeNextPlayer(&(position->hashKey));

    if (FUNC_RESULT_SUCCESS != result)
        return result;

    return PositionPlaceColor(position, move->RowIndex, move->ColumnIndex, SIDE_NONE);
}

FUNC_RESULT PositionDoMove(Position_T *position, const Move_T *move) {
    FUNC_RESULT result;

    if (NULL == position || !PositionCorrectMove(position, move))
        return FUNC_RESULT_FAILED_ARGUMENT;

    result = zobristHashChangeNextPlayer(&position->hashKey);

    if (FUNC_RESULT_SUCCESS != result)
        return result;

    if (!move->IsPass)
        result = PositionPlaceColor(position, move->RowIndex, move->ColumnIndex, move->Color);

    if (FUNC_RESULT_SUCCESS != result)
        return result;

    return PositionAddMoveToMoveHistory(position, move);
}

inline side_T PositionGet(const Position_T *position, size_t rowIndex, size_t columnIndex) {
    if (!PositionInside(position, rowIndex, columnIndex))
        return SIDE_NONE;
    return position->Position[PositionIndex(position, rowIndex, columnIndex)];
}

FUNC_RESULT PositionPrint(const Position_T *position, FILE *out) {
    const size_t WIDTH = 3, NUM_WIDTH = 3;
    side_T side;
    const char *symbol, *alphabet = "ABCDEFGHJKLMNOPQRSTUVWXYZ";

    if (NULL == position || NULL == out)
        return FUNC_RESULT_FAILED_ARGUMENT;

    fprintf(out, "\n%*s", NUM_WIDTH, "");
    for (size_t columnIndex = 0; columnIndex < position->ColumnsCount; ++columnIndex)
        fprintf(out, " %*c%*s", WIDTH / 2 + WIDTH % 2, alphabet[columnIndex], WIDTH / 2, "");
    fprintf(out, "\n");

    for (size_t rowIndex = 0; rowIndex < position->RowsCount + 1; ++rowIndex) {
        fprintf(out, "%*s", NUM_WIDTH, "");
        for (size_t j = 0; j < (WIDTH + 1) * position->ColumnsCount + 1; ++j)
            fprintf(out, "-");
        if (rowIndex == position->RowsCount)
            break;
        fprintf(out, "\n%-*d", NUM_WIDTH, position->RowsCount - rowIndex);
        for (size_t columnIndex = 0; columnIndex < position->ColumnsCount; ++columnIndex) {
            side = PositionGet(position, rowIndex, columnIndex);
            if (side == SIDE_BLACK)
                symbol = "X";
            else if (side == SIDE_WHITE)
                symbol = "O";
            else
                symbol = " ";
            fprintf(out, "|%*s%*s", WIDTH / 2 + WIDTH % 2, symbol, WIDTH / 2, "");
        }
        fprintf(out, "|%*d\n", NUM_WIDTH, position->RowsCount - rowIndex);
    }

    fprintf(out, "\n%*s", NUM_WIDTH, "");
    for (size_t columnIndex = 0; columnIndex < position->ColumnsCount; ++columnIndex)
        fprintf(out, " %*c%*s", WIDTH / 2 + WIDTH % 2, alphabet[columnIndex], WIDTH / 2, "");
    fprintf(out, "\n");

    return FUNC_RESULT_SUCCESS;
}

bool PositionIsGameOver(const Position_T *position, side_T *winner) {
    const Unit_T *unit;
    side_T _winner;

    if (NULL == position || position->MoveHistorySize == position->Size) {
        if (NULL != winner)
            *winner = SIDE_BOTH;
        return true;
    }

    _winner = SIDE_NONE;
    for (size_t index = 0; index < position->UnitsCount; ++index) {
        unit = &position->Units[index];
        if (unit->BlackCount == position->UnitLength) {
            _winner = SIDE_BLACK;
            break;
        }
        else if (unit->WhiteCount == position->UnitLength) {
            _winner = SIDE_WHITE;
            break;
        }
    }

    if (NULL != winner)
        *winner = _winner;
    return SIDE_NONE != _winner;
}

bool PositionMoveIsWinning(const Position_T *position, size_t rowIndex, size_t columnIndex, side_T color) {
    size_t cell, colorCount;
    const Unit_T *unit;

    if (NULL == position || !PositionInside(position, rowIndex, columnIndex) ||
        (SIDE_BLACK != color && SIDE_WHITE != color))
        return false;

    cell = PositionIndex(position, rowIndex, columnIndex);

    for (size_t index = 0; index < position->CellToUnitsCount[cell]; ++index) {
        unit = position->CellToUnits[cell][index];
        colorCount = SIDE_BLACK == color ? unit->BlackCount : unit->WhiteCount;
        if (UnitSide(unit) == color && colorCount == position->UnitLength - 1)
            return true;
    }

    return false;
}

FUNC_RESULT PositionGetCellValue(const Position_T *position, size_t rowIndex, size_t columnIndex, side_T side,
                                 value_T *value) {
    size_t cell;
    Unit_T *unit;

    if (NULL == position || !PositionInside(position, rowIndex, columnIndex) ||
        (SIDE_BLACK != side && SIDE_WHITE != side && SIDE_BOTH != side) || NULL == value)
        return FUNC_RESULT_FAILED_ARGUMENT;

    cell = PositionIndex(position, rowIndex, columnIndex);
    *value = 0;

    for (size_t index = 0; index < position->CellToUnitsCount[cell]; ++index) {
        unit = position->CellToUnits[cell][index];
        if (SIDE_BOTH == side || UnitSide(unit) == side || UnitSide(unit) == SIDE_NONE)
            *value += ABS(UnitValue(unit));
    }

    return FUNC_RESULT_SUCCESS;
}
